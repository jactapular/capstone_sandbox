#created manually - see cheat sheet for how to add a new app
from django.urls import path

from . import views

urlpatterns = [
    # example: .../search/
    path('', views.search_view, name='search_view'),

    # example: /search/panadol
    path('<str:search_term>/', views.results_view, name='results_view'),
]



# Use the following to add debug options to the urlpatterns
#    taken from dnt.dnt.urls.py
# if settings.DEBUG:
#     urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
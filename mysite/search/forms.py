from django import forms
from .models import SearchRequest

class SearchRequestForm(forms.ModelForm)

    class Meta: 
        model = SearchRequest
        fields = ('search_text')

    def save(self, commit=False):
        searchRequest = super(SearchRequestForm, self).save(commit=False)
        return searchRequest
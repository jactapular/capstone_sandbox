#default search of pbs database using the api's 'search' function, returns in json format
# built from the example curl request below, available at the pbs dev website
    # curl -d api_key=a643d03c2152a311e37f8e4a7afd43e6 "https://api.pbs.gov.au/0.3/search.xml?term=aspirin&effectivedate=2019-07-01&view=item"

# Example code for building a request url from: https://grantwinney.com/what-is-an-api-wrapper-and-how-do-i-write-one/
#   Modified to use python3 using the information at pythonprogramming.net/urllib-tutorial-python-3/

# TODO: 
# - add HTTP_404_Error exception handling for no results found
# - add some level of json parsing (i.e to a model object)

import urllib 
import json
import datetime

#calls the pbs api and returns the data as a json string
# throws an exception if no results found
def __call_api(endpoint):
    values = {'api_key': 'a643d03c2152a311e37f8e4a7afd43e6'}
    data = urllib.parse.urlencode(values)
    data = data.encode('utf-8')
    request = urllib.request.Request('https://api.pbs.gov.au%s' %(endpoint), data)
    response = urllib.request.urlopen(request)
    return json.loads(response.read())

# get an example result from the pbs database, takes no search term parameter
def show_example():
    result = __call_api('/0.3/search.json?term=aspirin&effectivedate=2019-07-01&view=item')
    return result


# return the json string from the api's search function for a given search parameter
def search(term):
    search_string = '/0.3/search.json?term=%s&effectivedate=2019-07-01&view=item' % term
    result = __call_api(search_string)
    return result



from django.shortcuts import render
from django.http import HttpResponse
from .forms import SearchForm
import search.dndb.dndb_api_wrapper as dndb


# home search page
# TODO: add template with search box and button
def search_view(request):
    form = SearchForm()
    return render(request, 'seach/search_home.html', {'form': form})


# show the results of searching the database
# TODO: format the returned json string, i.e. a scrollable list of result objects
def results_view(request, search_term):
    html = "<html><body>This is the results view. Search Results are:\n\n  %s.</body></html>"
    return HttpResponse(html % dndb.search(search_term))
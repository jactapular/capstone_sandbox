import datetime
from django.db import models
from django.utils import timezone

# Create your models here. Models are defined and incorporated 
# into the project with the Don't Repeat Yourself Principle.
# i.e. classes defined here will map directly to a db table 

#a single string search request
class SearchRequest(models.Model):
    search_text = models.CharField(max_length=200)
    #pub_date = models.DateTimeField('date published')
    def __str__(self):
        return self.search_text
    
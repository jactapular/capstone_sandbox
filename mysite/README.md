original site from polls tutorial 

to run:

	python manage.py runserver

to perform a search by url go to:

	http://127.0.0.1:8000/search/<search term>

	example 1 - searching a generic name:
	http://127.0.0.1:8000/search/aspirin/

	example 2 - searching a brand name:
	http://127.0.0.1:8000/search/Solpin/




Further notes:
	-> returns unformatted json results of all info, 
	OR A 404_Error if no data is found from pbs database
	-> search terms are not case sensitive
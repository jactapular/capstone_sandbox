========= Git - Standard =========

$ git clone https://bitbucket.org/jactapular/capstone_sandbox

$ git help everyday 

$ git add <file>

$ git status 

$ git commit -am <"Commit Message- i.e. Update README.md">

$ git push

$git config --global user.name "John Doe"

$git config --global user.email "johndoe@example.com"


======== Git - Undo a commit and redo ========                                                                                                                                                                                                                                
$ git commit ...                                                                                                                
$ git reset --soft HEAD^      (1)                                                                                               
$ edit                        (2)                                                                                               
$ git commit -a -c ORIG_HEAD  (3)                                                                                                                                                                                                                           
1. This is most often done when you remembered what you just committed is incomplete, or you misspelled your                    
commit message, or both. Leaves working tree as it was before "reset".                                                          
2. Make corrections to working tree files.                                                                                      
3. "reset" copies the old head to .git/ORIG_HEAD; redo the commit by starting with its log message. If you do not               
need to edit the message further, you can give -C option instead.


======== Django ==============

- Run the Server:
$ python3 manage.py runserver

- Adding an App to a project i.e. polls
$ python3 manage.py startapp polls



